### TransferTrash ###

![icon.png](https://bitbucket.org/repo/XdBy79/images/1231949527-icon.png)

**TransferTrash** es una aplicación para dispositivos Android diseñada para transferir de forma sencilla archivos desde tu móvil hasta un ordenador o cualquier otro tipo de dispositivo siempre que cuente con un navegador.

*En desarrollo*

     TransferTrash authors: Isaac Lacoba Molina Copyright (C) 2015

     This program is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License, or
     (at your option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with this program.  If not, see <http://www.gnu.org/licenses/>.