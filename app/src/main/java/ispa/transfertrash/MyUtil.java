package ispa.transfertrash;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

/**
 * Created by isaac on 17/09/15.
 */
public class MyUtil {
    public static boolean wifi_is_connected(Context context) {
        ConnectivityManager connManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return  mWifi.isConnected();
    }

    public static int get_ip(Context context) {
        WifiManager wifi_manager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        return wifi_manager.getConnectionInfo().getIpAddress();
    }

    public static String get_ip_address(Context context) {
        int ip = MyUtil.get_ip(context);
        return String.format("%d.%d.%d.%d", (ip & 0xff),(ip >> 8 & 0xff),
                (ip >> 16 & 0xff),(ip >> 24 & 0xff));
    }
}
