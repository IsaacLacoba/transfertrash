package ispa.transfertrash;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    private CheckBox server_cb, resume_cb;
    private TextView ip_tv, info_tv;
    private Server server;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ip_tv = (TextView) findViewById(R.id.server_link_tv);
        info_tv = (TextView) findViewById(R.id.info_tv);

        server = new Server();

        resume_cb = (CheckBox) findViewById(R.id.control_cb);
        server_cb = (CheckBox) findViewById(R.id.run_server_cb);
        server_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!MyUtil.wifi_is_connected(getApplicationContext())) {
                    info_tv.setText(R.string.error);
                    server_cb.setChecked(false);
                    return;
                }

                info_tv.setText(R.string.client_instructions);

                ip_tv.setText("ftp://" +
                        MyUtil.get_ip_address(getApplicationContext()) + ":2221");
                server_cb.setVisibility(View.INVISIBLE);
                resume_cb.setVisibility(View.VISIBLE);
                resume_cb.setText(R.string.suspend);

                server.start();
            }
        });

        resume_cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(server.is_suspended()){
                    server.resume();
                    resume_cb.setText(R.string.suspend);
                    return;
                }

                server.suspend();
                resume_cb.setText(R.string.resume);
                return;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
