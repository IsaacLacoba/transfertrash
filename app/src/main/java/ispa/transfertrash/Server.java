package ispa.transfertrash;

import android.util.Log;

import org.apache.ftpserver.ConnectionConfigFactory;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.listener.ListenerFactory;
import org.apache.ftpserver.ssl.SslConfigurationFactory;
import org.apache.ftpserver.usermanager.PropertiesUserManagerFactory;
import org.apache.ftpserver.usermanager.UserManagerFactory;
import org.apache.ftpserver.usermanager.impl.BaseUser;

import java.io.File;

/**
 * Created by isaac on 15/09/15.
 */
public class Server {
    private FtpServerFactory serverFactory;
    private ListenerFactory factory;
    private FtpServer server;

    public Server() {
        serverFactory = new FtpServerFactory();
        factory = new ListenerFactory();
        // set the port of the listener
        factory.setPort(2121);
        ConnectionConfigFactory connectionConfigFactory = new ConnectionConfigFactory();
        connectionConfigFactory.setAnonymousLoginEnabled(true);
        connectionConfigFactory.setMaxAnonymousLogins(2);
        serverFactory.setConnectionConfig(connectionConfigFactory.createConnectionConfig());

        BaseUser user = new BaseUser();
        user.setName("anonymous");
        user.setHomeDirectory("/storage");

        try {
            serverFactory.getUserManager().save(user);
        } catch (FtpException e) {
            e.printStackTrace();
        }

        // replace the default listener
        serverFactory.addListener("default", factory.createListener());
        // start the server
        server = serverFactory.createServer();
    }

    public void start() {
        try {
            Log.i("[Server]", "metodo start");
            server.start();
        } catch (FtpException e) {
            e.printStackTrace();
        }
    }

    public void suspend() {
        server.suspend();
    }

    public void resume() {
        server.resume();
    }

    public boolean is_suspended() {
        return server.isSuspended();
    }


}
